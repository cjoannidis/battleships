This is a simple API to allow a client to build a battleships game.

 How to run:

 ===========

 This is designed to be a pluggable API backend, so to use it you need to instantiate the GameController (examples
 of how can be found int GameSimulationControllerSpec).

 Requirements

 ============

 A player knows:

 - where their own ships are
 - where their own previous attacking moves have been made, and their result
 - where their opponent's previous attacking moves have been made and their result

 (These requirements are returned to the user after a successful operation, or by directly querying via the GameView object)

 The minimal set of operations we want to support:
 - Create an empty board.
 - Place a ship on the board.
 - Create a random board with the ships already placed.
 - Make an attacking move, determining if it is a hit or a miss and updating the game state.
 - Determine the current state of the game, finished (and who won), in play.

 Assumptions and Notes:

 ======================

 - Each player must register prior to placing their ships
 - Once both players have registered, each player must (independently) lay out their pieces either by creating a random board
   or placing each ship individually. Random boards can be modified by placing an already existing ship.
 - Once both boards are in a valid state the game can begin. Player 1 must make the first move followed by Player 2.
 - An attack move will either be successful, (in which case the client will receive their view of the game with
   their move applied), or it will fail. A failed move does not equal a turn. A player will only consume their turn when
   they have played a valid move. For the purposes of this simulation, it is considered an invalid move to attack a
   location that the player has previously attacked.
 - Once the game is finished, no further moves can be played but the clients will be able to interrogate the results
   by requesting the final state of the board.
 - If players lose track of the current state, they may at any time query the current state of the board.

 The API has been designed such that it can support any external interface or be used as if used in a .jar for example.
 It would be quite trivial for example to wrap the GameSimulationController with http routing logic to allow remote clients
 to play.

 I have taken care to ensure immutability where possible and build up higher order functions to get the right abstraction layer
 for some of the more complex operations. I have added a reasonable test suite to ensure the correct behavior of all non-trivial
 components and the overall api behaviour.

 Limitations

 ===========

 Despite best intentions, given the 6 hour time limit, there are a few limitations to the design that I would ideally
 like to improve upon if I had more time:
 - The GameState is unnecessarily coupled to the PlayerToken. It should ideally not know about it at all. (Further discussion
   about this in GameState.scala)
 - The GameState would ideally better enforce the contract of what moves can be made in different states. This is currently
   handled by the API.
 - The GameSimulationController has a mutable core state. Although the GameState object itself is immutable, the view that
   the controller holds is not, and thus exposes an interface which is mutable. There are a few ways to solve this:
   - Wrap all the mutable state in some abstraction such as Akka (though I would argue that this is introducing unnecessary
     complexity for this task, and does not really solve the core mutability problem (though it arguably does make the
     management of that mutability safer)).
   - Use something like the state monad to manage state manipulations, however this makes the API a bit clunkier (obviously
     this is a subjective viewpoint).
 - On the theme of mutability, registering players is a mutable stateful operation. Again, the above two solutions would be a
   possible solution, but for a first cut of this task, may not be necessary. The other alternative here might be to
   have a separate GameRegistrationController that handles this state and initialises the GameSimulationController with
   two valid, registered players.