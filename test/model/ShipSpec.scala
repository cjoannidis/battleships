package model

import org.scalatest.{Matchers, FlatSpec}

class ShipSpec extends FlatSpec with Matchers {

  it should "test a ship for position containment" in {
    val ship = Ship(Carrier, BoardPosition.from('a', 4).get, BoardPosition.from('a', 8).get)
    ship.contains(BoardPosition.from('a', 4).get) should be (true)
    ship.contains(BoardPosition.from('a', 5).get) should be (true)
    ship.contains(BoardPosition.from('a', 6).get) should be (true)
    ship.contains(BoardPosition.from('a', 7).get) should be (true)
    ship.contains(BoardPosition.from('a', 8).get) should be (true)
  }

  it should "classify non-contained points" in {
    val ship = Ship(Carrier, BoardPosition.from('a', 4).get, BoardPosition.from('a', 8).get)
    ship.contains(BoardPosition.from('c', 4).get) should be (false)
    ship.contains(BoardPosition.from('c', 5).get) should be (false)
    ship.contains(BoardPosition.from('c', 6).get) should be (false)
    ship.contains(BoardPosition.from('c', 7).get) should be (false)
    ship.contains(BoardPosition.from('c', 8).get) should be (false)
  }

  it should "test an overlapping ship for intersection" in {
    val ship = Ship(Carrier, BoardPosition.from('a', 4).get, BoardPosition.from('a', 8).get)
    ship.intersects(BoardPosition.from('a', 4).get, BoardPosition.from('a', 6).get) should be (true)
  }

  it should "test an crossing ship for intersection" in {
    val ship = Ship(Carrier, BoardPosition.from('c', 4).get, BoardPosition.from('c', 8).get)
    ship.intersects(BoardPosition.from('a', 5).get, BoardPosition.from('d', 5).get) should be (true)
  }

  it should "test an overlapping ship on its endpoint" in {
    val ship = Ship(Carrier, BoardPosition.from('c', 4).get, BoardPosition.from('c', 8).get)
    ship.intersects(BoardPosition.from('a', 8).get, BoardPosition.from('c', 8).get) should be (true)
  }

  it should "test a non overlapping ship for intersection" in {
    val ship = Ship(Carrier, BoardPosition.from('c', 4).get, BoardPosition.from('c', 8).get)
    ship.intersects(BoardPosition.from('a', 5).get, BoardPosition.from('a', 7).get) should be (false)
  }
}
