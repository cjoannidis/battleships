package model

import org.scalatest._

class BoardPositionSpec extends FlatSpec with Matchers {

  it should "generate all the positions between two points inclusive of endpoints left to right" in {
    val posx = (4 to 8).map(row => BoardPosition.from('a', row))
    (BoardPosition.allBetween(posx.head.get, posx.last.get) diff posx.map(_.get)) should be (Seq())
  }

  it should "generate all the positions between two points inclusive of endpoints right to left" in {
    val posx = (4 to 8).reverseMap(row => BoardPosition.from('c', row))
    (BoardPosition.allBetween(posx.head.get, posx.last.get) diff posx.map(_.get)) should be (Seq())
  }

  it should "generate all the positions between two points inclusive of endpoints top to bottom" in {
    val posx = ('a' to 'f').map(col => BoardPosition.from(col, 5))
    (BoardPosition.allBetween(posx.head.get, posx.last.get) diff posx.map(_.get)) should be (Seq())
  }

  it should "generate all the positions between two points inclusive of endpoints bottom to top" in {
    val posx = ('a' to 'f').reverseMap(col => BoardPosition.from(col, 7))
    (BoardPosition.allBetween(posx.head.get, posx.last.get) diff posx.map(_.get)) should be (Seq())
  }
}