import model._
import org.scalatest.{Matchers, FlatSpec}

class GameStateSpec extends FlatSpec with Matchers {

  private val fullFleet: Seq[ShipType] = Seq(Carrier, Battleship, Submarine, Cruiser, Patrol)

  it should "create a valid random board for player" in {
    val p1 = PlayerToken.generate
    val state = GameState.create(p1, PlayerToken.generate)
    val newState = state.randomBoardForPlayer(p1)

    newState.isSuccess should be (true)
    val ships = newState.get.player1Fleet.ships
    ships.tail.exists(ship => ship.intersects(ships.head.pos1, ships.head.pos2)) should be (false)
    ships.drop(1).tail.exists(ship => ship.intersects(ships.drop(1).head.pos1, ships.drop(1).head.pos2)) should be (false)
    ships.drop(2).tail.exists(ship => ship.intersects(ships.drop(2).head.pos1, ships.drop(2).head.pos2)) should be (false)
    ships.drop(3).tail.exists(ship => ship.intersects(ships.drop(3).head.pos1, ships.drop(3).head.pos2)) should be (false)
    ships.drop(4).tail.exists(ship => ship.intersects(ships.drop(4).head.pos1, ships.drop(4).head.pos2)) should be (false)
    newState.get.player1Fleet.ships.map(_.`type`) diff fullFleet should be (List())
  }

  it should "only allow the current player to attack on their turn" in {
    val p1 = PlayerToken.generate
    val p2 = PlayerToken.generate
    val state = GameState.create(p1, p2).randomBoardForPlayer(p1).flatMap(_.randomBoardForPlayer(p2))

    val illegalAttackAttempt = state.flatMap(_.attack(p2, BoardPosition.random))
    val legalAttackAttempt = state.flatMap(_.attack(p1, BoardPosition.random))
    val legalAttackAttempt2 = legalAttackAttempt.flatMap(_.attack(p2, BoardPosition.random))

    illegalAttackAttempt.isFailure should be (true)
    legalAttackAttempt.isSuccess should be (true)
    legalAttackAttempt2.isSuccess should be (true)
  }
}
