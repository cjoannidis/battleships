import model._
import org.scalatest.{Matchers, FlatSpec}

class GameSimulationControllerSpec extends FlatSpec with Matchers {

  private val fullFleet: Seq[ShipType] = Seq(Carrier, Battleship, Submarine, Cruiser, Patrol)

  it should "not let more than two players register for a game" in {
    val controller = GameSimulationController.createGame()
    val player1 = controller.registerPlayer()
    val player2 = controller.registerPlayer()
    val player3 = controller.registerPlayer()

    player1.isSuccess should be (true)
    player2.isSuccess should be (true)
    player3.isFailure should be (true)
  }

  it should "not let players perform any actions until all players have registered" in {
    val ctrl = GameSimulationController.createGame()
    val player1 = ctrl.registerPlayer().get

    ctrl.createEmptyBoard(player1).isFailure should be(true)
    ctrl.createRandomBoard(player1).isFailure should be(true)
    ctrl.placeShip(player1, Ship(Carrier, BoardPosition.from('a', 3).get, BoardPosition.from('a', 7).get)).isFailure should be(true)
    ctrl.attack(player1, BoardPosition.random).isFailure should be(true)
    ctrl.currentState(player1).isFailure should be(true)
  }

  it should "create an empty board" in {
    val state = withPlayers { (ctrl, player1, _) => {
      ctrl.createEmptyBoard(player1)
    }}

    state.isSuccess should be (true)
    state.get.playerFleet should be (Fleet.empty)
  }

  it should "create a valid random board" in {
    val state = withPlayers { (ctrl, player1, _) => {
      ctrl.createRandomBoard(player1)
    }}

    state.isSuccess should be (true)
    val ships = state.get.playerFleet.ships
    ships.tail.exists(ship => ship.intersects(ships.head.pos1, ships.head.pos2)) should be (false)
    ships.drop(1).tail.exists(ship => ship.intersects(ships.drop(1).head.pos1, ships.drop(1).head.pos2)) should be (false)
    ships.drop(2).tail.exists(ship => ship.intersects(ships.drop(2).head.pos1, ships.drop(2).head.pos2)) should be (false)
    ships.drop(3).tail.exists(ship => ship.intersects(ships.drop(3).head.pos1, ships.drop(3).head.pos2)) should be (false)
    ships.drop(4).tail.exists(ship => ship.intersects(ships.drop(4).head.pos1, ships.drop(4).head.pos2)) should be (false)
    state.get.playerFleet.ships.map(_.`type`) diff fullFleet should be (List())
  }

  it should "place a set of ships and update the game state accordingly" in {
    val state = withPlayers { (ctrl, player1, _) => {
      ctrl.placeShip(player1, ship1)
      ctrl.placeShip(player1, ship2)
    }}

    state.isSuccess should be (true)
    state.get.playerFleet.ships.size should be (2)
    state.get.playerFleet.ships should contain (ship1)
    state.get.playerFleet.ships should contain (ship2)
  }

  it should "not place a ship that intersects another ship" in {
    val state = withPlayers { (ctrl, player1, _) => {
      ctrl.placeShip(player1, ship2)
      ctrl.placeShip(player1, ship3)
    }}

    state.isFailure should be (true)
  }

  it should "not allow attacks until when boards aren't initialised" in {
    val state = withPlayers { (ctrl, player1, player2) => {
      ctrl.createRandomBoard(player1)
      ctrl.attack(player1, BoardPosition.from('a', 6).get)
    }}

    state.isFailure should be (true)
  }

  it should "allow attacks when boards are initialised" in {
    val state = withPlayers { (ctrl, player1, player2) => {
      ctrl.createRandomBoard(player1)
      ctrl.createRandomBoard(player2)
      ctrl.attack(player1, BoardPosition.from('a', 6).get)
    }}

    state.isSuccess should be (true)
  }

  it should "not count an invalid attack as a turn" in {
    val state = withPlayers { (ctrl, player1, player2) => {
      ctrl.createRandomBoard(player1)
      ctrl.createRandomBoard(player2)
      ctrl.attack(player1, BoardPosition.from('a', 6).get)
      ctrl.attack(player2, BoardPosition.from('a', 6).get)
      // Same attack as previously
      ctrl.attack(player1, BoardPosition.from('a', 6).get)
      // Attempt to attack again in a new spot
      ctrl.attack(player1, BoardPosition.from('b', 6).get)
    }}

    state.isSuccess should be (true)
  }

  private def withPlayers[A](f: (GameSimulationController, PlayerToken, PlayerToken) => A): A = {
    val ctrl = GameSimulationController.createGame()
    f(ctrl, ctrl.registerPlayer().get, ctrl.registerPlayer().get)
  }

  private val ship1 = Ship(Carrier, BoardPosition.from('a', 3).get, BoardPosition.from('a', 7).get)
  private val ship2 = Ship(Cruiser, BoardPosition.from('f', 7).get, BoardPosition.from('f', 8).get)
  private val ship3 = Ship(Submarine, BoardPosition.from('e', 7).get, BoardPosition.from('g', 7).get)
}
