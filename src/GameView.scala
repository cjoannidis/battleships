import model.{AttackResult, Fleet}

/**
 * A view of the current game that any given player has the authority to see
 */
case class GameView(playerFleet: Fleet,
                    playerMoves: Seq[AttackResult],
                    opponentMoves: Seq[AttackResult],
                    finished: Boolean,
                    winner: Option[Boolean])