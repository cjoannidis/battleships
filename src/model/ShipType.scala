package model

sealed trait ShipType {
  def name: String
  def size: Int
}

case object Patrol extends ShipType {
  override def name: String = "Patrol"
  override def size: Int = 1
}

case object Cruiser extends ShipType {
  override def name: String = "Cruiser"
  override def size: Int = 2
}

case object Submarine extends ShipType {
  override def name: String = "Submarine"
  override def size: Int = 3
}

case object Battleship extends ShipType {
  override def name: String = "Battleship"
  override def size: Int = 4
}

case object Carrier extends ShipType {
  override def name: String = "Carrier"
  override def size: Int = 5
}