package model

import java.util.UUID

case class PlayerToken(value: String) extends AnyVal

object PlayerToken {
  def generate = PlayerToken(UUID.randomUUID().toString)
}