package model

case class Fleet(ships: Seq[Ship])

object Fleet {
  def empty = Fleet(Seq())
}
