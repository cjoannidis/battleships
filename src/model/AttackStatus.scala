package model

sealed trait AttackStatus
case object Hit extends AttackStatus
case object Miss extends AttackStatus
