package model

/**
 * A simple representation of a ship on an abstract board
 */
case class Ship(`type`: ShipType, pos1: BoardPosition, pos2: BoardPosition) {

  def contains(pos: BoardPosition): Boolean = {
    (BoardPosition.allBetween(pos1, pos2) intersect List(pos)).size != 0
  }

  def intersects(p1: BoardPosition, p2: BoardPosition): Boolean = {
    (BoardPosition.allBetween(p1, p2) intersect BoardPosition.allBetween(pos1, pos2)).size != 0
  }
}

