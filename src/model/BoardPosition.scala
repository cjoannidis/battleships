package model

import scala.util.{Random, Try}

/**
 * Represents a valid position on the board
 * @param col The x position (from a - j)
 * @param row The y position (from 1 - 10)
 */
case class BoardPosition private(col: Char, row: Int) {
  import BoardPosition._
  
  require(col >= MIN_COL && col <= MAX_COL)
  require(row >= MIN_ROW && row <= MAX_ROW)
}

object BoardPosition {
  
  private val MIN_COL = 'a'
  private val MAX_COL = 'j'
  private val MIN_ROW = 1
  private val MAX_ROW = 10

  /**
   * Ensures that the constructed instance is valid and does not throw exceptions.
   */
  def from(col: Char, row: Int): Option[BoardPosition] = Try(BoardPosition(col, row)).toOption

  /**
   * Constructs a valid and Random BoardPosition
   */
  def random = BoardPosition(randomCol, randomRow)
  
  /**
   * Return all the points between the supplied endpoints (inclusive)
   */
  def allBetween(pos1: BoardPosition, pos2: BoardPosition): Seq[BoardPosition] = {
    val ptx = pos1 :: pos2 :: Nil
    if (pos1.row == pos2.row) {
      (ptx.minBy(_.col).col to ptx.maxBy(_.col).col).map(BoardPosition(_, pos1.row))
    } else {
      (ptx.minBy(_.row).row to ptx.maxBy(_.row).row).map(BoardPosition(pos1.col, _))
    }
  }

  private def randomCol = {
    val range = MAX_COL.toInt - MIN_COL.toInt
    (Random.nextInt(range + 1) + MIN_COL.toInt).toChar
  }
  private def randomRow = Random.nextInt(MAX_ROW) + MIN_ROW
}