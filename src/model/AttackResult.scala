package model

case class AttackResult(pos: BoardPosition, status: AttackStatus)
