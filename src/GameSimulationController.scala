import exception.{AllPlayersNotReadyException, NoRemainingSlotsException}
import model.{PlayerToken, Ship, BoardPosition}

import scala.util.{Failure, Success, Try}

/**
 * A simple API exposed to game clients.
 */
class GameSimulationController(player1: PlayerToken, player2: PlayerToken) {

  private var slotsRemaining = 2
  private var gameState: GameState = GameState.create(player1, player2)

  /**
   * Quick and dirty player auth scheme. Create a basic auth token to authenticate yourself.
   * Obviously this is not secure but it allows us to simply identify users when we can control the api consumers.
   * If we expect external users to call the api, then a more suitable auth scheme should be used.
   */
  def registerPlayer(): Try[PlayerToken] = {
    if (slotsRemaining > 0) {
      val tk = if (slotsRemaining == 2) Success(player1) else Success(player2)
      slotsRemaining = slotsRemaining - 1
      tk
    } else {
      Failure(new NoRemainingSlotsException())
    }
  }

  def createEmptyBoard(player: PlayerToken): Try[GameView] = {
    ifPlayersReady {
      updateState(SettingUp, gameState.emptyBoardForPlayer(player)).flatMap(_.viewForPlayer(player))
    }
  }

  def createRandomBoard(player: PlayerToken): Try[GameView] = {
    ifPlayersReady {
      updateState(SettingUp, gameState.randomBoardForPlayer(player)).flatMap(_.viewForPlayer(player))
    }
  }

  def placeShip(player: PlayerToken, ship: Ship): Try[GameView] = {
    ifPlayersReady {
      updateState(SettingUp, gameState.placeShipForPlayer(player, ship)).flatMap(_.viewForPlayer(player))
    }
  }

  def attack(player: PlayerToken, position: BoardPosition): Try[GameView] = {
    ifPlayersReady {
      if (gameState.isInitialised) {
        updateStateIfInEither(WaitingToStart, Started, gameState.attack(player, position)).flatMap(_.viewForPlayer(player))
      } else {
        updateState(Started, gameState.attack(player, position)).flatMap(_.viewForPlayer(player))
      }
    }
  }

  def currentState(player: PlayerToken): Try[GameView] = ifPlayersReady {
    gameState.viewForPlayer(player)
  }

  // Ensure that all the state changes happen in one place
  private def updateState(expectedState: CurrentState, f: => Try[GameState]): Try[GameState] = {
    if (expectedState == gameState.currentState) {
      val result = f
      result.foreach(newState => { gameState = newState })
      result
    } else {
      Failure(new IllegalStateException(s"Expected to be in state ${expectedState.toString} but was in ${gameState.currentState.toString}"))
    }
  }

  private def updateStateIfInEither(expectedState1: CurrentState,
                                    expectedState2: CurrentState,
                                    f: => Try[GameState]): Try[GameState] = {
    updateState(expectedState1, f).recoverWith { case _ => updateState(expectedState2, f)}
  }

  private def ifPlayersReady[A](f: => Try[A]): Try[A] = {
    if (slotsRemaining == 0) f else Failure(new AllPlayersNotReadyException())
  }
}

object GameSimulationController {
  def createGame(player1: PlayerToken = PlayerToken.generate,
                 player2: PlayerToken = PlayerToken.generate) = new GameSimulationController(player1, player2)
}