package exception

case class InvalidRequestException(reason: String) extends Exception
