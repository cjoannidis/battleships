import exception.{InvalidShipLocationException, InvalidMoveException, InvalidRequestException, InvalidPlayerException}
import model._

import scala.annotation.tailrec
import scala.util.{Random, Failure, Success, Try}


/**
 * Represents the current state of the game and contains functions that transform the current state into a
 * new state.
 *
 * This class currently only ensures that the ordering of turns is valid. It does not enforce the legality of moves
 * in a given state. This is currently handled by the API. Ideally that would be handled at this level though.
 * I would also remove the PlayerToken as something the game state knows about. This should not be necessary as
 * PlayerToken is an authentication mechanism for the API. The API would ideally authenticate the user and simply pass
 * in which user it wants to perform an action for.
 */
case class GameState(player1: PlayerToken, player1Fleet: Fleet, player1moves: Seq[AttackResult],
                     player2: PlayerToken, player2Fleet: Fleet, player2moves: Seq[AttackResult]) {

  private val fullFleet: Seq[ShipType] = Seq(Carrier, Battleship, Submarine, Cruiser, Patrol)

  val isInitialised = validFleet(player1Fleet) && validFleet(player2Fleet)
  private val isStarted: Boolean = player1moves.size > 0 || player2moves.size > 0
  private val isFinished: Boolean = finished(player1moves) || finished(player2moves)
  private val turn: PlayerToken = if (player1moves.size == player2moves.size) player1 else player2

  private val winner: Option[PlayerToken] = {
    if (finished(player1moves)) Some(player1)
    else if (finished(player2moves)) Some(player2)
    else None
  }
  
  private val player1View: GameView = GameView(player1Fleet, player1moves, player2moves, isFinished, winner.map(_ == player1))
  private val player2View: GameView = GameView(player2Fleet, player2moves, player1moves, isFinished, winner.map(_ == player2))
  
  val currentState: CurrentState = {
    if (!isInitialised && !isStarted && !isFinished) SettingUp
    else if (isInitialised && !isStarted && !isFinished) WaitingToStart
    else if(isStarted && !isFinished) Started
    else Finished
  }
  
  private def finished(moves: Seq[AttackResult]): Boolean = {
    val x = moves.count(_.status == Hit) == fullFleet.foldLeft(0)(_ + _.size)
    x
  }
  
  private def validFleet(fleet: Fleet) = {
    (fleet.ships.map(_.`type`) diff fullFleet).size == 0 && fleet.ships.size == fullFleet.size
  }

  def emptyBoardForPlayer(player: PlayerToken): Try[GameState] = {
    forPlayer(player)(this.copy(player1Fleet = Fleet.empty))(this.copy(player2Fleet = Fleet.empty))
  }

  def randomBoardForPlayer(player: PlayerToken): Try[GameState] = {
    forPlayer(player)(this.copy(player1Fleet = randomFleet))(this.copy(player2Fleet = randomFleet))
  }

  private def randomFleet: Fleet = {
    def generatePosition(placedShips: Seq[Ship], shipType: ShipType): Ship = {
      @tailrec
      def loop(pos1: BoardPosition): Ship = {
        // Random choose to go to the right or down
        val right = Random.nextBoolean()

        val pos2 = {
          if (right) Try(pos1.copy(col = (pos1.col.toInt + shipType.size - 1).toChar))
          else Try(pos1.copy(row = pos1.row + shipType.size - 1))
        }

        val isValid = (end: BoardPosition) => placedShips.forall(nextShip => !nextShip.intersects(pos1, end))

        if (pos2.filter(isValid).isSuccess) pos2.map(end => Ship(shipType, pos1, end)).get
        else loop(BoardPosition.random)
      }
      loop(BoardPosition.random)
    }

    Fleet(fullFleet.foldLeft(Seq[Ship]())((placedShips, shipType) => placedShips :+ generatePosition(placedShips, shipType)))
  }

  def placeShipForPlayer(player: PlayerToken, ship: Ship): Try[GameState] = {
    def tryUpdateFleet(fleet: Fleet): Try[Fleet] = {
      // remove the existing ship if it already exists (this will effectively make this a "move ship" operation in this case)
      val fleetWithoutDuplicateShip = Fleet(fleet.ships.filter(_.`type` != ship.`type`))
      val collision = fleetWithoutDuplicateShip.ships.exists(_.intersects(ship.pos1, ship.pos2))
      if (!collision) {
        Success(Fleet(fleetWithoutDuplicateShip.ships :+ ship))
      } else {
        Failure(new InvalidShipLocationException())
      }
    }
    for {
      newFleet <- forPlayer(player)(tryUpdateFleet(player1Fleet))(tryUpdateFleet(player2Fleet)).flatten
      newState <- forPlayer(player)(this.copy(player1Fleet = newFleet))(this.copy(player2Fleet = newFleet))
    } yield newState
  }

  def attack(player: PlayerToken, pos: BoardPosition): Try[GameState] = {
    for {
      currentState <- ensureTurn(player)
      newState <- attemptMove(player, pos)
    } yield newState
  }
  
  private def attemptMove(player: PlayerToken, move: BoardPosition): Try[GameState] = {
    val movesAndFleet = {
      forPlayer(player) {
        (player1moves, player2Fleet, (move: AttackResult) => this.copy(player1moves = player1moves :+ move))
      }{
        (player2moves, player1Fleet, (move: AttackResult) => this.copy(player2moves = player2moves :+ move))
      }
    }

    movesAndFleet.flatMap {
      case (moves, enemyFleet, updateFn) =>
        val historicMoves = moves.filter(_.pos != move)
        val status = if (enemyFleet.ships.exists(_.contains(move))) Hit else Miss
        if (historicMoves.size == moves.size) {
          Success(updateFn(AttackResult(move, status)))
        } else {
          Failure(new InvalidMoveException())
        }
    }
  }

  def viewForPlayer(player: PlayerToken): Try[GameView] = forPlayer(player)(player1View)(player2View)

  private def forPlayer[A](player: PlayerToken)
                          (player1Callback: => A)
                          (player2Callback: => A): Try[A] = player match {
    case `player1` => Success(player1Callback)
    case `player2` => Success(player2Callback)
    case _ => Failure(new InvalidPlayerException())
  }

  private def ensureTurn(player: PlayerToken): Try[GameState] = {
    if (player == turn) Success(this)
    else Failure(new InvalidRequestException("Not the current users turn to make a move"))
  }
}

object GameState {
  def create(player1: PlayerToken, player2: PlayerToken) = GameState(player1, Fleet.empty, Nil, player2, Fleet.empty, Nil)
}

sealed trait CurrentState
case object SettingUp extends CurrentState
case object WaitingToStart extends CurrentState
case object Started extends CurrentState
case object Finished extends CurrentState